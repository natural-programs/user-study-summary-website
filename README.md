# user-study-summary-website

Demo website with data from the user study.

## Live demo

A [live demo](https://natural-programs.gitlab.io/user-study-summary-website/) is available.

## Development instructions

### Requirements

- [Node.js](https://en.wikipedia.org/wiki/Node.js)

### Interactive app

```bash
npm install
npm run dev
```

## Deployment instructions

An example Gitlab CI script ([.gitlab-ci.yml](.gitlab-ci.yml)) is provided.

The database may have to be properly configured (e.g., set CORS headers and
access rules in Firebase).
