"""Extract to CSV the performance in the given chain data obtained through the
download button in the aggregation plots page."""
import json
import csv
import argparse
import json
from pathlib import Path

parser = argparse.ArgumentParser(
    description='Extract node performance from chain data.'
)
parser.add_argument(
    '--input_json',
    type=Path,
    help='Aggregation data in JSON format.',
    required=True,
)
parser.add_argument(
    '--output_csv',
    type=Path,
    help='Output CSV file that will be created.',
    required=True,
)
parser.add_argument(
    '--max_generation',
    type=int,
    help='Only include data about generations less or equal to this value (0 indexed).',
    required=True,
)
args = parser.parse_args()

# Open JSON containing a list of chains
with open(args.input_json) as fp:
    chains_data = json.load(fp)
assert type(chains_data) == list, "Aggregate data should be a list"

# Assemble chain data
data = list()
for chain_data in chains_data:
    for node in chain_data["data"]:
        node_data = dict(
            chain_seed=node["chain_seed"],
            user_id=node["user_id"],
            mode=node["mode"],
            sat_items_n=node["sat_items_n"],
            total_submissions=len(node["submissions"]),
            first_try_goal_item_n=node["first_try_goal_item_n"],
            crafted_goal_item_n=node["crafted_goal_item_n"],
            previous_unique_successful_concrete_programs_n=node["previous_unique_successful_concrete_programs_n"],
        )
        data.append(node_data)

with open(args.output_csv, "wt") as fp:
    fieldnames = [
        "chain_seed",
        "mode",
        "user_id",
        "sat_items_n",
        "total_submissions",
        "first_try_goal_item_n",
        "crafted_goal_item_n",
        "previous_unique_successful_concrete_programs_n",
    ]
    writer = csv.DictWriter(fp, fieldnames=fieldnames)

    writer.writeheader()
    for datum in data:
        if datum["user_id"] <= args.max_generation:
            writer.writerow(datum)

print(f"Wrote {args.output_csv}")
