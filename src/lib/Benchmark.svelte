<script lang="ts">
	import type { FailedExecution } from "$lib/firebase";
	import type { NaturalProgramSketch } from "natural-programs/types";
	import { SessionType, type BeamSearchParameters, type SessionSnapshot } from "./session_snapshot";
	import type { TopLevelSpec } from "vega-lite";
	import vegaEmbed from "vega-embed";
	import { get_snapshot, type SuccessfulExecution } from "$lib/firebase";
	import { get_api, get_concrete_beam, get_np_beam, preprocess_np_for_search, type BeamParameters, type ProgramState } from "./minecraft_app";
	import { parse_csvs } from "./world_csv";
	import { onDestroy } from 'svelte';
	import { solve_with_np } from "./workers";
	import { solve_with_synthesis } from "./workers";
	import { keys } from "natural-programs/library/library";
	import type { ProposeParameters } from "natural-programs/library/proposers";

	export let executions: ((SuccessfulExecution|FailedExecution)&{node_i: number})[];
	export let np_solver_parameters: BeamSearchParameters;
	export let synthesis_solver_parameters: BeamSearchParameters;
	export let solver_phase_timeout: number;
	export let propose_parameters: ProposeParameters;
	export let np_solver_beam_parameters: BeamParameters;
	export let synthesis_solver_beam_parameters: BeamParameters;
	export let max_queue_size: number;

	type Result = {
		source: string,
		library_size: number,
		milliseconds: number,
		dequeue_n: number,
		error: boolean,
		node_i: number,
	};

	let execution_progress = 0;
	let results: Result[][] = [];
	let cancel_execution = false;
	onDestroy(() => {
		cancel_execution = true;
	});

	function embed(node: HTMLElement, vega_specification: TopLevelSpec) {
		const div_id = node.id;

		const spec: TopLevelSpec = {
			...vega_specification,
			width: "container",
			height: "container",
		};
		const result = vegaEmbed('#'+div_id, spec, {renderer: "svg"});
		return {
			destroy() { result.then(r=>r.finalize()); }
		}
	}

	function filter_successful_results(
			results: Result[][]
			): Result[][] {
		return results.map(rs=>rs.filter(r=>!(r.error)));
	}

	function filter_failed_results(
			results: Result[][]
			): Result[][] {
		return results.map(rs=>rs.filter(r=>(r.error)));
	}

	function get_solver_time_distribution_plot(
			results: Result[][]
			): TopLevelSpec {
		const data = results.flatMap(
			rs=>rs.map(r=>{return {library_size: r.library_size, source: r.source, milliseconds: r.milliseconds}})
		);
		return {
			data: {values: data},
			mark: "point",
			encoding: {
				x: {field: "library_size"},
				y: {field: "milliseconds", type: "quantitative"},
				color: {field: "source"},
			}
		}
	}

	function get_dequeue_distribution_plot(
			results: Result[][]
			): TopLevelSpec {
		const data = results.flatMap(
			rs=>rs.map(r=>{return {library_size: r.library_size, source: r.source, dequeue_n: r.dequeue_n}})
		);
		return {
			data: {values: data},
			mark: "point",
			encoding: {
				x: {field: "library_size"},
				y: {field: "dequeue_n", type: "quantitative"},
				color: {field: "source"},
			}
		}
	}

	function get_solver_distribution_plot(
			data: Result[]
			): TopLevelSpec {
		return {
			data: {values: data},
			transform: [{
					groupby: ["source"],
					sort: [{field: "library_size"}],
					window: [{op: "count", field: "count", as: "Cumulative Count"}],
				}],
				mark: {type: "area", line: true, point: true, interpolate: "step-after", "opacity": 0.5},
			encoding: {
				x: {field: "library_size", type: "quantitative"},
				y: {field: "Cumulative Count", type: "quantitative"},
				color: {field: "source"},
			}
		}
	}

	function get_generation_plot(
			results: Result[][]
			): TopLevelSpec {
		const data = results.flatMap(
			rs=>rs.map(r=>{return {library_size: r.library_size, source: r.source, milliseconds: r.milliseconds, generation_i: r.node_i}})
		);
		return {
			data: {values: data},
			mark: "point",
			encoding: {
				x: {field: "library_size"},
				y: {field: "generation_i", type: "quantitative"},
			}
		}
	}

	async function init() {
		for (const execution of executions) {
			const snapshot = await get_snapshot(execution.execution_snapshot_id, "");
			const synthesis_problem = {
				np: JSON.parse(execution.np),
				snapshot: snapshot,
			}
			const problem_results = await solve(synthesis_problem, execution.node_i);
			execution_progress = execution_progress + 100/executions.length;
			results = [...results, problem_results];
			if (cancel_execution)
				return;
		}
		execution_progress = 100;
	}

	async function solve(
			problem: {np: NaturalProgramSketch, snapshot: SessionSnapshot},
			node_i: number,
			): Promise<Result[]> {
		const state: ProgramState = {
			inventory: problem.snapshot.inventory,
			crafting_table: [],
			held_item: null,
		};
		const np = problem.np;
		const library = problem.snapshot.library;
		const world = parse_csvs(problem.snapshot.item_csv, problem.snapshot.mine_csv, problem.snapshot.seed, "");
		const api = get_api(world);
		const results = [];
		const search_np = await preprocess_np_for_search(np, library);

		const np_promise = solve_with_np({
			np: search_np,
			state: state,
			beam: await get_np_beam(
				search_np,
				library,
				np_solver_parameters.library_beam_width,
				np_solver_beam_parameters,
			),
			parameters: np_solver_parameters,
			world: world,
			api: api,
			library: library,
			timeout_ms: solver_phase_timeout,
			propose_parameters: propose_parameters,
			max_queue_size: max_queue_size,
			fringe_seed: [], // TODO, track this data too!
		}, null);

		const synthesis_promise = solve_with_synthesis({
			np: search_np,
			state: state,
			beam: await get_concrete_beam(
				search_np,
				library,
				synthesis_solver_parameters.library_beam_width,
				synthesis_solver_beam_parameters,
			),
			parameters: synthesis_solver_parameters,
			world: world,
			api: api,
			library: library,
			timeout_ms: solver_phase_timeout,
			propose_parameters: propose_parameters,
			max_queue_size: max_queue_size,
			fringe_seed: [], // TODO, track this data too!
		}, null);

		{
			const og_result = await np_promise;
			const result = {
				dequeue_n: og_result.dequeue_n,
				milliseconds: og_result.milliseconds,
				library_size: keys(library).length,
				source: SessionType.NaturalProgramming,
				error: "error" in og_result,
				node_i: node_i,
			};

			// Log result
			results.push(result);
		}

		{
			// Try direct synthesis solver
			const og_result = await synthesis_promise;
			const result = {
				dequeue_n: og_result.dequeue_n,
				milliseconds: og_result.milliseconds,
				library_size: keys(library).length,
				source: SessionType.DirectSynthesis,
				error: "error" in og_result,
				node_i: node_i,
			};

			// Log result
			results.push(result);
		}

		return results;
	}

	init();
</script>

{#if execution_progress < 100}
	<p>Solving synthesis problems...</p>
	<progress class="progress is-medium is-primary" value={execution_progress} max="100">{execution_progress}%</progress>
{:else}
	<p>Done!</p>
{/if}

<div class="content">
	<p>Notes</p>
	<ul>
		<li>
			Two solvers are benchmarked independently on each synthesis problem:
			<ol>
				<li>Natural Programming solver</li>
				<li>Traditional synthesis solver</li>
			</ol>
		</li>
		<li>Each solver has (approximately and in addition to termination conditions imposed by the hyperparameters) the following timeout: {solver_phase_timeout}ms.</li>
	</ul>
</div>

{#key results}
	<table class="table">
		<thead><th>Metric</th><th>Value</th></thead>
		<tbody>
			{#each [SessionType.NaturalProgramming, SessionType.DirectSynthesis] as source}
			<tr>
					<td>Successful {source}</td>
					<td>{filter_successful_results(results).flat().filter(r=>r.source===source).length}</td>
				</tr>
				<tr>
					<td>Failed {source}</td>
					<td>{filter_failed_results(results).flat().filter(r=>r.source===source).length}</td>
				</tr>
			{/each}
		</tbody>
	</table>

	<h3 class="subtitle">Successful time distribution</h3>
	<p>How much time was spent in successful execution.</p>
	<div class="vega">
		<div
			class="inner-vega"
			id={"vega_lite_success_solver_distribution"}
			use:embed={get_solver_time_distribution_plot(filter_successful_results(results))}
			>
		</div>
	</div>

	<h3 class="subtitle">Failed time distribution</h3>
	<p>How much time was spent in failed executions.</p>
	<div class="vega">
		<div
			class="inner-vega"
			id={"vega_lite_failed_solver_distribution"}
			use:embed={get_solver_time_distribution_plot(filter_failed_results(results))}
			>
		</div>
	</div>

	<h3 class="subtitle">Cumulative successes</h3>
	<p>Solver success events.</p>
	<div class="vega">
		<div
			class="inner-vega"
			id={"vega_lite_success_cause_solver_distribution"}
			use:embed={get_solver_distribution_plot(filter_successful_results(results).flat())}
			>
		</div>
	</div>

	<h3 class="subtitle">Cumulative failures</h3>
	<p>Solver failure events.</p>
	<div class="vega">
		<div
			class="inner-vega"
			id={"vega_lite_failure_cause_solver_distribution"}
			use:embed={get_solver_distribution_plot(filter_failed_results(results).flat())}
			>
		</div>
	</div>

	<h3 class="subtitle">Dequeue success distribution</h3>
	<p>How many dequeues were performed in successful execution.</p>
	<div class="vega">
		<div
			class="inner-vega"
			id={"vega_lite_success_dequeue_distribution"}
			use:embed={get_dequeue_distribution_plot(filter_successful_results(results))}
			>
		</div>
	</div>

	<h3 class="subtitle">Dequeue failed distribution</h3>
	<p>How many dequeues were performed in failed executions.</p>
	<div class="vega">
		<div
			class="inner-vega"
			id={"vega_lite_failed_dequeue_distribution"}
			use:embed={get_dequeue_distribution_plot(filter_failed_results(results))}
			>
		</div>
	</div>

	<h3 class="subtitle">Generations</h3>
	<p>Library sizes to generation numbers.</p>
	<div class="vega">
		<div
			class="inner-vega"
			id={"vega_lite_generations"}
			use:embed={get_generation_plot(results)}
			>
		</div>
	</div>
{/key}

<style>
	.vega {
		height: 50vh;
		width: 80vw;
	}
	.inner-vega {
		width: 100%;
		height: 100%;
	}
</style>
