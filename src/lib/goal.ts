import type { Item } from "minecraft/items";
import { parse_csvs } from "./world_csv";
import { parse_csvs_all_recipes } from "./world_csv";
import { appears_in_recipe } from "minecraft/items";
import { Chance } from "chance";
import type { Goal } from "./minecraft_app";

enum GoalSource {
	AllCraftableItems = "ALL_CRAFTABLE",
	CurrentTreeLeafs = "TREE_LEAFS",
	MetaLeafs = "META_LEAFS",
};

type GoalParameters = {
	item_n: number,
	copy_n: number
	source: GoalSource,
};

/**
 * Return all craftable items.
 */
function get_craftable_items(
		item_csv: string,
		mine_csv: string,
		seed: string,
		): Item [] {
	// Parse csvs
	const world = parse_csvs(
		item_csv,
		mine_csv,
		seed,
		"",
	);
	return [...world.name_to_item.values()]
		.filter(i=>"recipe" in i);
}

/**
 * Return all craftable items that are leafs
 * in the world induced by the seed.
 */
function get_leaf_items(
		item_csv: string,
		mine_csv: string,
		seed: string,
		): Item [] {
	// Parse csvs
	const world = parse_csvs(
		item_csv,
		mine_csv,
		seed,
		"",
	);
	return [...world.name_to_item.values()]
		.filter(i=>"recipe" in i)
		.filter(i=>!appears_in_recipe(i, world));
}

/**
 * Return all craftable items that are leafs
 * in all worlds induced by the CSV.
 */
function get_meta_leaf_items(
		item_csv: string,
		mine_csv: string,
		seed: string,
		): Item [] {
	const all_recipes = parse_csvs_all_recipes(
		item_csv,
		"",
	);
	const candidates = get_craftable_items(
		item_csv,
		mine_csv,
		seed
	);
	const meta_leafs = [];
	for (const item of candidates) {
		const recipes_with_item = all_recipes
			.filter(item2=>"recipe" in item2 && item2.recipe.includes(item.name));
		if (recipes_with_item.length === 0)
			meta_leafs.push(item);
	}
	return meta_leafs;
}

/**
 * Return a random goal.
 */
function get_random_goal(
		item_csv: string,
		mine_csv: string,
		seed: string,
		parameters: GoalParameters,
		): Goal {
	// Filter items that are craftable (and, if required, are also leafs)
	const candidate_items =
		(parameters.source === GoalSource.AllCraftableItems) ?
		get_craftable_items(item_csv, mine_csv, seed) :
		(parameters.source === GoalSource.CurrentTreeLeafs) ?
		get_leaf_items(item_csv, mine_csv, seed) :
		get_meta_leaf_items(item_csv, mine_csv, seed);

	if (parameters.item_n > candidate_items.length)
		throw "Cannot generate goal of " + parameters.item_n + " items by choosing between " + candidate_items.length + " items!";

	// Build goal by selecting a random subset of items
	const goal = [];
	const chance = new Chance(seed);
	const items: ([Item, number][]) = chance
		.shuffle(candidate_items.map((v, i)=>[v, i]));
	for (let i = 0; i < parameters.item_n; i++)
		for (let j = 0; j < parameters.copy_n; j++)
			goal.push(items[i]);
	goal.sort(([_, n1], [__, n2])=>n1-n2);
	return goal.map(([i, _])=>i);
}

/**
 * Get goal seed for given chain seed and chain height.
 */
function get_goal_seed(chain_seed: string, chain_height: number): string {
	return JSON.stringify([chain_seed, chain_height]);
}

export {
  get_random_goal,
  GoalSource,
	get_goal_seed,
};

export type {
  GoalParameters,
};
