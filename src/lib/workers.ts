import type { SolverResult } from "natural-programs/types";
import type { SolverError } from "natural-programs/types";
import type { ProgramState } from "./minecraft_app";
import type { SynthesisProblem } from "./minecraft_app";
import NPWorker from "./np_worker?worker";
import SynthesisWorker from "./synthesis_worker?worker";

type Result = (SolverResult<ProgramState>|SolverError<ProgramState>);
let np_worker = new NPWorker();
let synthesis_worker = new SynthesisWorker();

/**
 * Solve the synthesis problem with the natural programming solver in a
 * different thread.
 */
async function solve_with_np(
		synthesis_problem: SynthesisProblem,
		cancel_promise: Promise<void>|null,
		): Promise<Result> {
	console.log({submitted: synthesis_problem});
	return new Promise(function(resolve, reject) {
		np_worker.addEventListener('message', function(message) {
			resolve(message.data);
		});
		if (cancel_promise !== null)
			cancel_promise.then(() => {
				np_worker.terminate();
				np_worker = new NPWorker();
				reject();
			});
		np_worker.postMessage(synthesis_problem);
	});
}

/**
 * Solve the synthesis problem with the direct synthesis solver in a different
 * thread.
 */
async function solve_with_synthesis(
		synthesis_problem: SynthesisProblem,
		cancel_promise: Promise<void>|null,
		): Promise<Result> {
	console.log({submitted: synthesis_problem});
	return new Promise(function(resolve, reject) {
		synthesis_worker.addEventListener('message', function(message) {
			resolve(message.data);
		});
		if (cancel_promise !== null)
			cancel_promise.then(() => {
				synthesis_worker.terminate();
				synthesis_worker = new SynthesisWorker();
				reject();
			});
		synthesis_worker.postMessage(synthesis_problem);
	});
}

function create_np_worker(): Worker { return new NPWorker(); }

/**
 * Solve the synthesis problem with the given worker.
 */
async function solve_with_worker(
		synthesis_problem: SynthesisProblem,
		worker: Worker,
		): Promise<Result> {
	console.log({submitted: synthesis_problem});
	return new Promise(function(resolve) {
		worker.addEventListener('message', function(message) {
			resolve(message.data);
		});
		worker.postMessage(synthesis_problem);
	});
}

export {
	solve_with_synthesis,
	solve_with_np,
	solve_with_worker,
	create_np_worker,
}
