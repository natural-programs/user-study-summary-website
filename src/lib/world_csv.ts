import Papa from "papaparse";
import type { CraftingWorld } from "minecraft/items";
import type { Item } from "minecraft/items";
import { Chance } from "chance";

/**
 * Parse the Mining yields CSV.
 */
function parse_mining_yields(mine_csv: string): Map<(string|null), Map<string, number>> {
	const yields = Papa.parse(mine_csv.trim(), {header: true});
	// (tool_id|null)->(mined_item_id->yield_n)
	const mining_yields = new Map<(string|null), Map<string, number>>();
	for (const row of yields.data) {
		let tool_id = (row as any).tool_id.trim();
		const item_id = (row as any).item_id.trim();
		const yield_n = Number((row as any).yield_n);
		if (tool_id.length === 0)
			tool_id = null;
		let tool_yields = mining_yields.get(tool_id);
		if (tool_yields === undefined)
			tool_yields = new Map<string, number>();
		tool_yields.set(item_id, yield_n);
		mining_yields.set(tool_id, tool_yields);
	}
	return mining_yields;
}

/**
 * Parse all recipes in the Item CSV into a mapping of the form `key ->
 * recipes`. Item CSVs can contain multiple recipe columns (with `recipe` as a
 * substring of the name).
 *
 * Returns a list with item names in the order of the CSV and a mapping from
 * item name to item data.
 *
 * Example CSV:
 *
 * ```csv
 * item_id,is_infinite,is_tool,is_material,recipe1,recipe2
 * wood,1,,1,,
 * stone,1,,1,,
 * wool,1,,1,,
 * grass,1,,1,,
 * wood_plank,,,1,"wood, stone","wood, wood"
 * bed,,,1,"wood_plank, wool","wood, wool"
 * wood_stick,,,1,"wood_plank, wood_plank","wood_plank, stone"
 * ```
 */
function parse_recipes(
		item_csv: string,
		icon_directory: string, // with trailing slash
		): [string[], Map<string, {item: Item, recipes: string[]}>] {
	const items = Papa.parse(item_csv.trim(), {header: true});

	const fields = items.meta.fields;
	if (fields === undefined)
		throw "CSV parsing failed!";

	// Identify CSV column names about recipes
	const recipe_fields = fields.filter(s=>s.includes("recipe"));
	if (recipe_fields.length === 0)
		throw "CSV does not contain column names with 'recipe' as sub-string!";

	const recipes = new Map<string, {item: Item, recipes: string[]}>();
	const item_names = [];

	for (const row of items.data) {
		const row_data = row as any;
		const item_name = row_data.item_id.trim();
		item_names.push(item_name);
		let item: Item = {
			name: item_name,
			is_tool: row_data.is_tool === "1",
			score: 0,
			is_infinite: row_data.is_infinite === "1",
			icon_src: icon_directory + item_name + ".png",
		};
		if (item.is_tool)
			item = {...item, durability: 1};

		// Parse all recipes in this column of the CSV
		const recipe_strings = recipe_fields
			.map(recipe_column=>row_data[recipe_column]);
		const item_recipes = recipe_strings
			// Split string into item names
			.map(recipe_string=>recipe_string.split(","))
			// Courtesy removal of empty item names
			.map(recipe_array=>recipe_array.filter((n: string)=>n.length>0))
			// Courtesy sanitization of item names
			.map(recipe_array=>recipe_array.map((name: string)=>name.trim()))
			// Remove empty recipes (which in the CSV represent non-existent recipes)
			.filter(recipe=>recipe.length>0);

		recipes.set(item_name, {item: item, recipes: item_recipes});
	};
	return [item_names, recipes];
}

/**
 * Parse CSVs into a crafting world. Item CSVs can contain multiple recipe
 * columns (with `recipe` as a substring of the name).
 *
 * The given seed is used to choose between the available recipes. If a
 * non-empty recipe is chosen, the item will be constructed as a CraftableItem.
 */
function parse_csvs(
		item_csv: string,
		mine_csv: string,
		seed: string,
		icon_directory: string, // with trailing slash
		): CraftingWorld<Item> {
	const data = parse_recipes(item_csv, icon_directory);
	const item_names = data[0];
	const recipes = data[1];
	const items = [];
	const chance = new Chance(seed);
	for (const item_name of item_names) {
		const raw_item = recipes.get(item_name);
		if (raw_item === undefined)
			throw "Cannot find item data which is assumed to be in CSV!";
		if (raw_item.recipes.length > 0) {
			// Choose a recipe
			const recipes = raw_item.recipes;
			const i = chance.natural({min: 0, max: recipes.length-1});
			const recipe = recipes[i];
			const item = {
				...raw_item.item,
				recipe: recipe,
			};
			items.push(item);
		} else {
			items.push(raw_item.item);
		}
	};

	const mining_yields = parse_mining_yields(mine_csv);
	const name_to_item = new Map<string, Item>(items.map(i=>[i.name, i]));
	return {
		name_to_item: name_to_item,
		mining_yields: mining_yields
	};
}

/**
 * Parse items in the given item CSV. Item CSVs can contain multiple recipe
 * columns (with `recipe` as a substring of the name).
 *
 * All recipes will be used. I.e. if an item has 3 recipes, the crafting world
 * will contain three Item objects with identical name and different recipes.
 */
function parse_csvs_all_recipes(
		item_csv: string,
		icon_directory: string, // with trailing slash
		): Item[] {
	const data = parse_recipes(item_csv, icon_directory);
	const items = [];
	const item_names = data[0];
	const recipes = data[1];
	for (const item_name of item_names) {
		const raw_item = recipes.get(item_name);
		if (raw_item === undefined)
			throw "Cannot find item data which is assumed to be in CSV!";
		if (raw_item.recipes.length > 0)
		for (const recipe of raw_item.recipes) {
			const item = {
				...raw_item.item,
				recipe: recipe,
			};
			items.push(item);
		} else {
			items.push(raw_item.item);
		}
	};
	return items;
}

export {
	parse_csvs,
	parse_csvs_all_recipes,
};
