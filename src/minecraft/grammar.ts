/**
 * Defines a grammar for Minecraft crafting.
 *
 * S := pick(ITEM_ID) | place(POS, POS) | craft()
 * ITEM_ID := 0 | ... | ~200
 * POS := 0 | 1 | 2
 *
 */
import type { Item } from "./items";

enum MinecraftSimplePrimitive {
	Craft = "COLLECT",
	Clear = "CLEAR",
};

type MinecraftPrimitivePick = {
	item: Item,
};

type MinecraftPrimitiveMine = {
	mined_item: Item,
};

type MinecraftPrimitive = (
	MinecraftSimplePrimitive
	| MinecraftPrimitivePick
	| MinecraftPrimitiveMine
);


/*
 * A Program is a list containing either Primitives or Programs
 */
type MinecraftProgram = {
	statements: (MinecraftPrimitive|MinecraftProgram)[],
};


/**
 * A HierarchicalPrimitive is either a MinecraftProgram or a
 * MinecraftPrimitive.
 */
type MinecraftHierarchicalPrimitive = (MinecraftPrimitive|MinecraftProgram);

export type {
	MinecraftHierarchicalPrimitive,
	MinecraftPrimitive,
	MinecraftPrimitivePick,
	MinecraftPrimitiveMine,
	MinecraftProgram,
}

export {
	MinecraftSimplePrimitive,
}
