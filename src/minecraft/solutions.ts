/**
 * Functions to generate data for free.
 **/
import type { Item } from "../minecraft/items";
import type { MinecraftSimplePrimitive } from "./grammar";

/**
 * Return a human-readable string representing the pick operation.
 */
function naturalize_pick(item: Item): string {
	const name = item.name.toLowerCase();
	return "place " + name;
}

/**
 * Return a human-readable string representing the mine operation.
 */
function naturalize_mine(mined_item: Item): string {
	const name = mined_item.name.toLowerCase();
	return "mine " + name;
}

/**
 * Return a human-readable string representing the primitive operation.
 */
function naturalize_simple_primitive(primitive: MinecraftSimplePrimitive): string {
	return primitive.toLowerCase();
}

export {
	naturalize_pick,
	naturalize_mine,
	naturalize_simple_primitive,
};
