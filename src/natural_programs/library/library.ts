import { get_embeddings, cosine_similarity } from "nlp";
import type { NaturalProcedureSketch, NaturalProgramSketch } from "natural-programs/types";
import type { Callstack } from "natural-programs/types";
import type { NaturalProcedure } from "natural-programs/types";
import type { NaturalSignature } from "natural-programs/types";
import type { Primitive } from "natural-programs/types";
import type { StepsCollection } from "natural-programs/library/steps_collection";
import { add_to_collection } from "natural-programs/library/steps_collection";
import { get_empty_steps_collection } from "natural-programs/library/steps_collection";
import { get_key as get_decomposition_key } from "natural-programs/library/steps_collection";

type ItemID = number;
type CanonicalCallstack = [ItemID, number][];

/**
 * Helper function to try the embedding end-point a couple of times to reduce
 * impact of network errors.
 */
async function get_embedding(query: string, timeout: number) {
	const attempt_n = 10;
	for(let i = 0; i < attempt_n; i++) {
		try {
			const val = (await get_embeddings([query], timeout))[0];
			return val;
		} catch(e) {
			// This happens e.g. when script is run without network access or the
			// API endpoint is down.
			console.error({attempt: [i+1,attempt_n].join('/'), error: e});
		}
	}
	throw 'Failed to get string embedding!';
}

/**
 * Return all non-empty suffixes of an array, from small to big.
 */
function get_suffixes<T>(a: T[]): T[][] {
	if(a.length === 0) return [[]];
	const a_r = [...a];
	let last_suffix: T[] = [];
	const suffixes = [last_suffix];
	a_r.reverse();
	for(const ai of a_r) {
		last_suffix = [ai, ...last_suffix];
		suffixes.push(last_suffix);
	}
	return suffixes;
}

/**
 * Return all natural language strings in the natural programs.
 */
function get_natural_strings(nps: NaturalProgramSketch[]): string[] {
	const nl_strings = new Set<string>();

	// Keep track of unique visited NPs
	const visited_nps = new Set<string>(nps.map(np=>JSON.stringify(np)));
	const queue = [...visited_nps].map(key=>JSON.parse(key));

	while (true) { // while queue is not empty
		const np = queue.shift();

		if (np === undefined) // queue is empty
			return [...nl_strings];

		if (typeof np === "string") {
			// Primitive case
			nl_strings.add(np);
		} else if (!("steps" in np)) {
		// Signature case
			if (np.name !== undefined)
				nl_strings.add(np.name);
		} else {
			// Procedure case
			if (np.signature.name !== undefined)
				nl_strings.add(np.signature.name);
			for (const step of np.steps) {
				const key = JSON.stringify(step);
				if (!(visited_nps.has(key))) {
					visited_nps.add(key);
					queue.push(step);
				}
			}
		}
	}
}

/**
 * Collection of NaturalProcedureSketch and execution traces. The purpose of
 * this data structure is two-fold:
 *   1. Consistent ID assignment to any NaturalSignature and Primitive.
 *   2. Fast look-up of queries of the form "what programs have been executed
 *      under this context?".
 */
type Library = {
	added_sketches: NaturalProgramSketch[];
	items: Set<NaturalSignature|Primitive>;
	items_keys: Set<string>; // for fast queries of "is this item in library?"
	ids_to_items: Map<ItemID, (NaturalSignature|Primitive)>;
	item_to_ids: Map<string, ItemID>;
	signatures_to_id_sequences: Map<ItemID, Map<string, {sequence: ItemID[], frequency: number}>>;
	item_vectors: Map<ItemID, number[]>;
	post_condition_items: Map<string, Map<ItemID, number>>;
	ids_to_concrete_nps: Map<ItemID, NaturalProcedure[]>;
	recently_used_decompositions: StepsCollection,
	recently_used_ids: number[],
	max_recently_used_decompositions: number,
};

/**
 * Returns an empty library.
 */
function new_library(): Library {
	return {
		added_sketches: [],
		items: new Set<NaturalSignature|Primitive>(),
		items_keys: new Set<string>(),
		ids_to_items: new Map<ItemID, (NaturalSignature|Primitive)>(),
		item_to_ids: new Map<string, ItemID>(),
		signatures_to_id_sequences: new Map<ItemID, Map<string, {sequence: ItemID[], frequency: number}>>(),
		item_vectors: new Map<ItemID, number[]>(),
		post_condition_items: new Map<string, Map<ItemID, number>>(),
		ids_to_concrete_nps: new Map<ItemID, NaturalProcedure[]>(),
		recently_used_decompositions: get_empty_steps_collection(),
		recently_used_ids: [],
		max_recently_used_decompositions: 500,
	}
}

/**
 * Returns the ID assigned in the library to the given program, or undefined
 * if no such key exists.
 */
function get_id(library: Library, key: (NaturalSignature|Primitive)): ItemID | undefined {
	const k =  JSON.stringify(key);
	return library.item_to_ids.get(k);
}

/**
 * Returns the item assigned to the given ID, or undefined if no such
 * program exists.
 */
function get(library: Library, id: ItemID): (NaturalSignature|Primitive) | undefined {
	return library.ids_to_items.get(id);
}

/**
 * Returns list of IDs for items in the library.
 */
function keys(library: Library): ItemID[] {
	return [...library.items].map(np=>{
		return get_id(library, np) as ItemID;
	});
}

/**
 * Returns list of items in the library.
 */
function values(library: Library): (NaturalSignature|Primitive)[] {
	return [...library.items];
}

/**
 * Returns the sketches added to the library.
 */
function get_added_sketches(library: Library): NaturalProgramSketch[] {
	return [...library.added_sketches];
}

/**
 * Adds the given item to the library and returns it's unique ID. If the
 * library already contains the item this method simply returns it's ID.
 *
 * A timeout for the embedding API must be provided.
 */
async function _add(library: Library, item: (NaturalSignature|Primitive), timeout: number): Promise<ItemID> {
	const id = assign_id(library, item);
	const k = JSON.stringify(item);

	// Check if the item is already in the library
	if (library.items_keys.has(k))
		return id;

	if (typeof item === "string" || item.name !== undefined) {
		// Record name embedding
		const name = (typeof item === "string") ? item : item.name as string;
		const vector = (await get_embedding(name, timeout));
		library.item_vectors.set(id, vector);
	}

	if (typeof item !== "string" && "post_condition" in item) {
		const post_condition = item.post_condition;
		if (post_condition !== undefined) {
			// Record post condition
			const maybe_items = library.post_condition_items.get(post_condition);
			const items = (maybe_items !== undefined) ? maybe_items : new Map<ItemID, number>();
			if (maybe_items === undefined)
				library.post_condition_items.set(post_condition, items);
			const maybe_frequency = items.get(id);
			const frequency = (maybe_frequency !== undefined) ? maybe_frequency : 0;
			items.set(id, frequency+1);
		}
	}
	library.items.add(item);
	library.items_keys.add(k);
	return id;
}

/**
 * Assigns a unique ID's to the given program and returns it. If the
 * program already had an ID, this method simply returns it.
 */
function assign_id(library: Library, item: (NaturalSignature|Primitive)): ItemID {
	const id = get_id(library, item);
	if(id === undefined) {
		const id = library.ids_to_items.size;
		const k = JSON.stringify(item);
		library.item_to_ids.set(k, id);
		library.ids_to_items.set(id, item);
		return id;
	} else {
		return id;
	}
}

/**
 * Returns the canonical representation of the given call-stack.
 */
function get_canonical_callstack(library: Library, callstack: Callstack): CanonicalCallstack {
	const canonical_callstack: CanonicalCallstack = [];
	for(const [signature, line_i] of callstack) {
		const i = assign_id(library, signature);
		canonical_callstack.push([i, line_i]);
	}
	return canonical_callstack;
}

/**
 * Return the sequences of item IDs items that have been recorded under the
 * exact given call-stack.
 */
function get_sequences(
		library: Library,
		id: ItemID,
		): {sequence: ItemID[], frequency: number}[] {
	const svalues = library.signatures_to_id_sequences.get(id);
	if (svalues === undefined)
		return [];
	return [...svalues.values()];
}

/**
 * Return the ID's of item IDs that have the same post-condition.
 */
function get_ids_with_post_condition(library: Library, post_condition: string): Set<ItemID> {
	const values = library.post_condition_items.get(post_condition);
	if (values !== undefined)
		return new Set([...values.keys()]);
	return new Set<ItemID>();
}

/**
 * Identity function if the program is a concrete procedure, undefined otherwise.
 */
function as_concrete(np: NaturalProgramSketch): NaturalProcedure | string | undefined {
	if (typeof np === "string")
		return np;
	if (typeof np === "string" || !("steps" in np))
		return undefined;
	const steps: (NaturalProcedure|string)[] = [];
	for (const step of np.steps) {
		const concrete_step = as_concrete(step);
		if (concrete_step === undefined)
			return undefined;
		steps.push(concrete_step);
	}
	return {...np, steps: steps};
}

/**
 * Recursively add every (signature, [...steps]) tuple in the
 * procedure, adding the programs to the library.
 *
 * A timeout for the embedding API must be provided.
 */
async function add_recursive(library: Library, np: NaturalProgramSketch, timeout: number) {
	// Update recency trackers
	{
		if (typeof np !== "string" && ("steps" in np))
			add_to_collection(
				library.recently_used_decompositions,
				np,
				library.max_recently_used_decompositions
			);
		if (typeof np !== "string") {
			const item = ("steps" in np) ? np.signature : np;
			const sigkey = assign_id(library, item);
			library.recently_used_ids.push(sigkey);
		}
	}

	library.added_sketches.push(np);
	if (typeof np === "string" || !("steps" in np))
		return await _add(library, np, timeout);

	async function cs_helper(np: NaturalProcedureSketch) {
		const sigkey = await _add(library, np.signature, timeout);

		// Construct the sequence of step IDs
		const id_sequence = [];
		for (const step of np.steps) {
			const item = (typeof step === "string") ? step : ("steps" in step) ? step.signature : step;
			const id = await _add(library, item, timeout);
			id_sequence.push(id);
		}

		// Record the decomposition
		let id_sequences = library.signatures_to_id_sequences.get(sigkey);
		if (id_sequences === undefined) {
			id_sequences = new Map<string, {sequence: ItemID[], frequency: number}>();
			library.signatures_to_id_sequences.set(sigkey, id_sequences);
		}
		const sequence_key = JSON.stringify(id_sequence);
		const old_frequency = id_sequences.get(sequence_key);
		if (old_frequency === undefined)
			id_sequences.set(sequence_key, {sequence: id_sequence, frequency: 1});
		else
			id_sequences.set(sequence_key, {sequence: id_sequence, frequency: old_frequency.frequency+1});

		for (let i = 0; i < np.steps.length; i++) {
			const step = np.steps[i];
			if (typeof step !== "string" && ("steps" in step)) {
				await cs_helper(step);
			}
		}
	}
	await cs_helper(np);

	// Record concrete implementations if program is concrete
	async function record_concrete_implementations(np: NaturalProcedure) {
		const sigkey = await _add(library, np.signature, timeout);
		// Record the concrete implementation
		const previous_implementations = library.ids_to_concrete_nps.get(sigkey);
		if (previous_implementations === undefined) {
			library.ids_to_concrete_nps.set(sigkey, [np]);
		} else {
			const np_string = JSON.stringify(np);
			const previous_np_strings = new Set<string>(
					previous_implementations.map(np=>JSON.stringify(np))
				);
			if (!(previous_np_strings.has(np_string)))
				previous_implementations.push(np);
		}
		for (let i = 0; i < np.steps.length; i++) {
			const step = np.steps[i];
			if (typeof step !== "string") {
				await record_concrete_implementations(step);
			}
		}
	}
	const concrete_np = as_concrete(np);
	if (concrete_np !== undefined && typeof concrete_np !== "string")
		await record_concrete_implementations(concrete_np);
}

/**
 * Call `add_recursive` on each program. This function should be used when
 * adding many programs to a library, as it batches all calls to
 * `get_embeddings` into a single call.
 */
async function add_recursive_batch(library: Library, programs: NaturalProgramSketch[], string_sim_timeout: number): Promise<Library> {
	// Embed strings all natural language strings in a single request to reduce
	// overhead from HTTP requests
	const nl_strings = get_natural_strings(programs);
	(await get_embeddings(nl_strings, string_sim_timeout*nl_strings.length));

	// Actually de-serialize library
	for (const concrete_np of programs)
		await add_recursive(library, concrete_np, string_sim_timeout);
	return library;
}

/**
 * Return the IDs of the top-k named programs in the library according to
 * string similarity between their names and the given query, and their
 * corresponding similarities.
 *
 * The array is sorted from high to low similarity.
 *
 * A timeout for the embedding API must be provided.
 */
async function get_k_nearest(
		library: Library,
		query: string,
		k: number|null,
		timeout: number
		): Promise<[ItemID, number][]> {
	// This would be faster with a kd-tree
	const query_vector = (await get_embedding(query, timeout));
	const named_program_ids = [...library.item_vectors.entries()];
	const named_program_similarities = named_program_ids.map(function([id, vec]): [number, number] {
		return [id, cosine_similarity(vec, query_vector)];
	});
	named_program_similarities.sort(function([_, sim_a], [__, sim_b]) {
		return sim_b - sim_a;
	});
	if (k !== null)
		return named_program_similarities.slice(0, k);
	return named_program_similarities;
}

type CompressedLibrary = {
	indexes: number[],  // Sequence of sketches added to the library
	stringified_added_sketches: string[],
};

/**
 * Return a JSON string which can be used to restore the state of the
 * library.
 */
function as_json(library: Library): string {
	const sketch_to_index = new Map<string, number>();
	const sketches = [];
	const indexes = [];
	let i = 0;
	for (const sketch of library.added_sketches) {
		const key = JSON.stringify(sketch);
		const maybe_index = sketch_to_index.get(key);
		const index = (maybe_index === undefined) ? i : maybe_index;
		if (maybe_index === undefined) {
			sketches.push(sketch);
			sketch_to_index.set(key, index);
			i++;
		}
		indexes.push(index);
	}
	const compressed_library: CompressedLibrary = {
		indexes: indexes,
		stringified_added_sketches: sketches.map(np=>JSON.stringify(np)),
	}
	return JSON.stringify(compressed_library);
}

/**
 * Restore the state of the library using the given JSON string.
 */
async function from_json(json: string, string_sim_timeout: number): Promise<Library> {
	const library = new_library();
	let json_object = JSON.parse(json);

	// Legacy support: previously libraries were stored as a raw array
	if (Array.isArray(json_object))
		return add_recursive_batch(library, json_object, string_sim_timeout);

	// Legacy support: previously sketches were not stringified
	if (!("stringified_added_sketches" in json_object))
		json_object = {
			...json_object,
			stringified_added_sketches: json_object.added_sketches.map((np: any)=>JSON.stringify(np))
		};

	// Reconstruct the array
	const compressed_library = json_object as CompressedLibrary;
	const parsed_sketches = compressed_library.stringified_added_sketches.map(np=>JSON.parse(np));
	const added_sketches = compressed_library.indexes.map(i=>parsed_sketches[i]);
	return add_recursive_batch(library, added_sketches, string_sim_timeout);
}

/**
 * Return the concrete implementations of the given signature.
 */
function get_concrete_implementations(library: Library, signature: NaturalSignature): NaturalProcedure[] {
	const id = get_id(library, signature);
	if (id === undefined)
		return [];
	const implementations = library.ids_to_concrete_nps.get(id);
	if (implementations === undefined)
		return [];
	return implementations;
}

/**
 * Return a score for the given ID: 0.0 (never or oldest
 * used) to 1.0 (most recently used).
 */
function get_recency_score(library: Library, item: (Primitive|NaturalSignature)): number {
	// Return a binary score.
	const id = assign_id(library, item);
	// Maybe make the size of recent items a tunable parameter
	const recent_items = library.recently_used_ids.slice(-10, 0);
	return recent_items.includes(id) ? 1.0 : 0.0;
}

/**
 * Return a linear recency score for the given ID, from 0.0 (never or oldest
 * used) to 1.0 (most recently used).
 */
function get_decomposition_recency_scores(
		library: Library,
		decompositions: ((Primitive|NaturalSignature)[])[]
		): number[] {
	// Could be optimized to perform a single traversal instead of three
	// Get temporal indices of each decomposition
	const temporal_indices = new Map<string, number>();
	const keys = decompositions.map(get_decomposition_key);
	for (const key of keys) {
		const t = library.recently_used_decompositions.temporal_indices.get(key);
		if (t !== undefined)
			temporal_indices.set(key, t);
	}

	// Normalize temporal indices
	const min_t = Math.min(...temporal_indices.values());
	const max_t = Math.max(...temporal_indices.values());
	function normalize(t: number|undefined): number {
		if (t === undefined) return 0;
		if (max_t === min_t) return 1;
		return (t-min_t)/(max_t-min_t);
	}

	return keys.map(key=>normalize(temporal_indices.get(key)));
}

export type {
	ItemID,
	Library,
};

export {
	new_library,
	get_suffixes,
	get_natural_strings,
	as_json,
	add_recursive,
	add_recursive_batch,
	keys,
	get_k_nearest,
	get_sequences,
	from_json,
	values,
	get_canonical_callstack,
	get_ids_with_post_condition,
	get,
	get_id,
	get_concrete_implementations,
	assign_id,
	get_recency_score,
	get_decomposition_recency_scores,
	get_added_sketches,
};
