/**
 * Simple interface to record "recently used" Natural Program steps.
 */

import type { NaturalProcedureSketch, NaturalSignature, Primitive } from "natural-programs/types";

type StepsCollection = {
	temporal_indices: Map<string, number>; // Current temporal indices of decompositions recorded
	max_index: number; // total number of decompositions recorded
}

/**
 * Return an empty step collection.
 */
function get_empty_steps_collection(): StepsCollection {
	return {
		temporal_indices: new Map<string, number>(),
		max_index: 0,
	};
}

/**
 * Return a string representation of the given sequence of steps.
 */
function get_key(steps: (Primitive|NaturalSignature)[]): string {
	return JSON.stringify(steps);
}

/**
 * Recursively add all step sequences to the collection. This is O(n) on the
 * size of the collection.
 */
function add_to_collection(
		collection: StepsCollection,
		np: NaturalProcedureSketch,
		max_size: number,
		) {
	// Transform the top-level steps into a collection key. If steps have further
	// steps (i.e. are not signatures or primitives), record them to recurse
	// at the end.
	const steps: (Primitive|NaturalSignature)[] = [];
	const recurse: NaturalProcedureSketch[] = [];
	for (const step of np.steps) {
		if (typeof step === "string")
			steps.push(step);
		else if (!("steps" in step))
			steps.push(step);
		else {
			steps.push(step.signature);
			recurse.push(step);
		}
	}
	const key = get_key(steps);

	// Add the key to the collection
	const t = collection.max_index+1;
	collection.max_index = t;
	collection.temporal_indices.set(key, t);

	// Remove oldest decomposition if size exceeds limit. If this routine is too
	// slow, it can be reduced to O(logn) popping oldest index with a priority
	// queue.
	while (collection.temporal_indices.size > max_size) {
		let oldest_key = key;
		let min_temporal_index = t;
		for (const [key2, t2] of collection.temporal_indices.entries()) {
			if (t2 < min_temporal_index) {
				oldest_key = key2;
				min_temporal_index = t2;
			}
		}
		collection.temporal_indices.delete(oldest_key);
	}

	// Recurse in each procedure step
	for (const step of recurse)
		add_to_collection(collection, step, max_size);
}

export {
	add_to_collection,
	get_empty_steps_collection,
	get_key,
};

export type {
	StepsCollection
};
