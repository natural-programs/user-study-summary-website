import type { NaturalProcedure } from "natural-programs/types";

/**
 * Return the sequence of primitives in the concrete program.
 */
function get_unrolled_implementation(np: NaturalProcedure|string): string[] {
	if (typeof np === "string")
		return [np];
	return np.steps.flatMap(get_unrolled_implementation);
}

export {
	get_unrolled_implementation
};
